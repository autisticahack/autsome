angular.module('starter.controllers', [])

.controller('DashCtrl', function ($scope, $rootScope, Actions, $state, $ionicPopup, $http, $cordovaCamera) {
  $scope.actions = Actions.all();

  $scope.editAction = function (selected) {

    $rootScope.selectedAction = selected;
    $state.go("tab.editActions");
  };


  $scope.getAll = function () {
    $http.get(apiUrl + "/actions")
      .then(function (response) {
        $scope.actions = response.data.actions;
      });
  };

  $scope.getAll();


  $scope.takePhoto = function () {
    var options = {
      quality: 75,
      destinationType: Camera.DestinationType.DATA_URL,
      sourceType: Camera.PictureSourceType.CAMERA,
      allowEdit: true,
      encodingType: Camera.EncodingType.JPEG,
      targetWidth: 300,
      targetHeight: 300,
      popoverOptions: CameraPopoverOptions,
      saveToPhotoAlbum: false
    };


    $cordovaCamera.getPicture(options).then(function (imageData) {
      $scope.imgURI = "data:image/jpeg;base64," + imageData;
      $scope.dataTobeUploaded = imageData;
      $scope.uploadImage();
      

    }, function (err) {
      // An error occured. Show a message to the user
    });

  }

  $scope.choosePhoto = function () {
    var options = {
      quality: 75,
      destinationType: Camera.DestinationType.DATA_URL,
      sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
      allowEdit: true,
      encodingType: Camera.EncodingType.JPEG,
      targetWidth: 300,
      targetHeight: 300,
      popoverOptions: CameraPopoverOptions,
      saveToPhotoAlbum: false
    };

    $cordovaCamera.getPicture(options).then(function (imageData) {
      $scope.imgURI = "data:image/jpeg;base64," + imageData;
    }, function (err) {
      // An error occured. Show a message to the user
    });
  };

  $scope.takeAction = function (selected) {

    if (selected.targetAction === 'callNumber') {
      // alert('Calling Someone');
      window.location.href = "tel:+91 9922533180";
    }
    if (selected.targetAction === 'showImage') {
      window.location= "https://www.youtube.com/watch?v=ziGG_L9C12o";
    }
    if (selected.targetAction === 'playAudio') {
      alert('Playing Audio');
    }
    if (selected.targetAction === 'playVideo') {
      alert('Playing Video');
    }
    if (selected.targetAction === 'showFeeling') {
       $scope.takePhoto();
      
    }
  };



    $scope.uploadImage = function () {
        $http({
        method: 'POST',
        url: 'https://api.imgur.com/3/image',
        data: {
          image: $scope.dataTobeUploaded
        },
        headers: {
          "Authorization": 'Client-ID ce9af36aa1182ad'
        }
      }).success(function (response) {
            $scope.uploadedImage = response.data.link;
            $scope.emotionApi(response.data.link);

      }).error(function (response) {
        $rootScope.emotions = response;
        $state.go("tab.playAudio");
      });
    };

    $scope.emotionApi = function (link) {
          var data = {"url": link};
            $http({
              method: 'POST',
              url: 'https://api.projectoxford.ai/emotion/v1.0/recognize',
              data: data,
              headers: {
                "ocp-apim-subscription-key": "2ca6d854922c44c0b201306790a8d8f6",
                "content-type": "application/json",
                "cache-control": "no-cache"
              }
            }).success(function (response) {
              // alert("Happiness is " + response[0].scores.happiness * 100);
              // alert("Sadness is " + response[0].scores.sadness * 100);
               $rootScope.emotions = response[0].scores;

               $rootScope.state = "";

               if((response[0].scores.happiness > response[0].scores.sadness) && (response[0].scores.happiness > response[0].scores.neutral)) {
                    $rootScope.state = "happy";
               }

               else if((response[0].scores.sadness > response[0].scores.happiness)  && (response[0].scores.sadness > response[0].scores.neutral)) {
                    $rootScope.state = "sad";
               }

               
               else if((response[0].scores.neutral > response[0].scores.happiness)  && (response[0].scores.neutral > response[0].scores.sadness)) {
                    $rootScope.state = "neutral";
               }

               $state.go("tab.playAudio");
            }).error(function (response) {
              $rootScope.emotions = response;
              $state.go("tab.playAudio");
            });
    };
})



.controller('ActionsCtrl', function ($scope, Actions, $state) {
  $scope.action = {};
  $scope.action.targetAction = 'callNumber';
  $scope.action.face = 'img/call.png';

  $scope.addAction = function () {
    if ($scope.action.name) {
      Actions.add($scope.action);
      $state.go("tab.dash", {}, {
        reload: true
      });
    } else {
      alert("Please add a name");
    }

  }

  $scope.updateImage = function () {
    console.log($scope.action.targetAction);
    if ($scope.action.targetAction === 'callNumber') {
      $scope.action.face = 'img/call.png';
    }
    if ($scope.action.targetAction === 'showImage') {
      $scope.action.face = 'img/Show.png';
    }
    if ($scope.action.targetAction === 'playAudio') {
      $scope.action.face = 'img/audio.png';
    }
    if ($scope.action.targetAction === 'playVideo') {
      $scope.action.face = 'img/video-play.png';
    }

    if ($scope.action.targetAction === 'showFeeling') {
      $scope.action.face = 'img/feeling.png';
    }

  };
})

.controller('EditActionsCtrl', function ($scope, $rootScope) {
  console.log($rootScope.selectedAction);
  $scope.remove = function (chat) {
    Actions.remove(chat);
  };
})

.controller('tabsController', function ($scope, $rootScope, $state) {
  $scope.getDashboard = function () {
    $state.go("tab.dash", {}, {
      reload: true
    });
  };
})


.controller('PlayAudioCtrl', function ($scope, $rootScope) {

})

.controller('ChatDetailCtrl', function ($scope, $stateParams, Chats) {
  $scope.chat = Chats.get($stateParams.chatId);
})

.controller('AccountCtrl', function ($scope) {
  $scope.settings = {
    enableFriends: true
  };
});
