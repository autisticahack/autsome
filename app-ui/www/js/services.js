angular.module('starter.services', []).factory('Actions', ['$rootScope','$http',function($rootScope, $http) {

  
  // Some fake testing data
  var actions = [{
    id: 0,
    name: 'Call a Number',
    face: 'img/call.png',
    targetAction:'callNumber'
  }, {
    id: 1,
    name: 'Show an Image',
    face: 'img/Show.png',
    targetAction:'showImage'
  }, {
    id: 2,
    name: 'Play Audio',
    face: 'img/audio.png',
    targetAction:'playAudio'
  }, {
    id: 3,
    name: 'Play Video',
    face: 'img/video-play.png',
    targetAction:'playVideo'
  }];

  return {
    all: function() {
      $http.get(apiUrl + "/actions")
    .then(function(response) {
      // console.log(response.data.actions);
        return response.data.actions;
    });

      // return actions;
    },
    remove: function(action) {
      $http.post(apiUrl + "/actions?id=1")
      .then(function(response) {
          return response;
      });
    },
    add: function(action) {
      $http.post(apiUrl + "/actions/create", JSON.stringify(action))
      .then(function(response) {
         alert ("Action added !");
          return response;
      });
    },
    update: function(action) {
      $http.post(apiUrl + "/actions?id=1",action)
      .then(function(response) {
          return response;
      });
    },
    get: function(chatId) {
      for (var i = 0; i < chats.length; i++) {
        if (chats[i].id === parseInt(chatId)) {
          return chats[i];
        }
      }
      return null;
    },
     uploadToimgur: function(imageData) {
      $http(
        {
          method:'POST',
          url:'http://api.imgur.com/3/image',
          headers:{
            Authorization:'Client-ID ce9af36aa1182ad'
          },
          data:{image:imageData}
        }
      ).then(function(response){
        alert(response.data.link);
      })
    }
  };

}]);



// angular.module('starter.services', [$http])

// .factory('Actions', function($http) {
//   // Might use a resource here that returns a JSON array

//   // Some fake testing data
//   var actions = [{
//     id: 0,
//     name: 'Call a Number',
//     face: 'img/call.png',
//     targetAction:'callNumber'
//   }, {
//     id: 1,
//     name: 'Show an Image',
//     face: 'img/Show.png',
//     targetAction:'showImage'
//   }, {
//     id: 2,
//     name: 'Play Audio',
//     face: 'img/audio.png',
//     targetAction:'playAudio'
//   }, {
//     id: 3,
//     name: 'Play Video',
//     face: 'img/video-play.png',
//     targetAction:'playVideo'
//   }];

//   return {
//     all: function() {
//       $http.get(apiUrl + "/actions?id=1")
//     .then(function(response) {
//         console.log(response);
//     });

//       return actions;
//     },
//     remove: function(action) {
//       chats.splice(chats.indexOf(chat), 1);
//     },
//     add: function(action) {
//       chats.splice(chats.indexOf(chat), 1);
//     },
//     update: function(action) {
//       chats.splice(chats.indexOf(chat), 1);
//     },
//     get: function(chatId) {
//       for (var i = 0; i < chats.length; i++) {
//         if (chats[i].id === parseInt(chatId)) {
//           return chats[i];
//         }
//       }
//       return null;
//     }
//   };
// });
