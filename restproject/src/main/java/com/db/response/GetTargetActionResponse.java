package com.db.response;

import com.db.dto.Action;
import com.db.model.Status;

import java.util.Collection;

public class GetTargetActionResponse extends Response {

    private final Collection<Action> actions;

    public Collection<Action> getActions() {
        return actions;
    }

    public GetTargetActionResponse(Collection<Action> actions, Status status) {
        super(status);
        this.actions = actions;
    }
}
