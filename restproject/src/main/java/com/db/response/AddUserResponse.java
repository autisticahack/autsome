package com.db.response;


import com.db.model.Status;

public class AddUserResponse {

    private String userId;
    private Status status;

    public AddUserResponse(String userId, Status status) {
        this.userId = userId;
        this.status = status;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
