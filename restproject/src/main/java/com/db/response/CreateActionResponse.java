package com.db.response;

import com.db.model.Status;

/**
 * Created by iDiot on 11/10/16.
 */
public class CreateActionResponse extends Response{
    private String actionId;

    public CreateActionResponse(String actionId,Status status) {
        super(status);
        this.actionId = actionId;
    }

    public String getActionId() {
        return actionId;
    }
}
