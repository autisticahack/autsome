package com.db.response;

import com.db.dto.Action;
import com.db.model.Status;

/**
 * Created by iDiot on 11/10/16.
 */
public class GetActionResponse extends Response{

    private final Action action;

    public GetActionResponse(Action action, Status status) {
        super(status);
        this.action = action;
    }

    public Action getAction() {
        return action;
    }
}
