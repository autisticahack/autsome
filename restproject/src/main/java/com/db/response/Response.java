package com.db.response;

import com.db.model.Status;

/**
 * Created by iDiot on 11/10/16.
 */
public class Response {
    private Status status;

    public Response(Status status) {
        this.status = status;
    }

    public Status getStatus() {
        return status;
    }
}
