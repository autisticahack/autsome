package com.db.response;

import com.db.dto.Action;
import com.db.model.Status;

import java.util.Collection;

/**
 * Created by iDiot on 11/10/16.
 */
public class GetAllActionResponse extends Response {

    private final Collection<Action> actions;

    public Collection<Action> getActions() {
        return actions;
    }

    public GetAllActionResponse(Collection<Action> actions, Status status) {
        super(status);
        this.actions = actions;
    }

}
