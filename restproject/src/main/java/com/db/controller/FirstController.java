package com.db.controller;

import com.db.dao.UserDao;
import com.db.dto.UserDto;
import com.db.model.Status;
import com.db.response.AddUserResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Rahul on 11/9/2016.
 */
@RestController
public class FirstController {

    private UserDao userDao;

    @Autowired
    public FirstController(UserDao userDao) {
        this.userDao = userDao;
    }

    @RequestMapping("/addUser")
    public AddUserResponse addUser(@RequestParam("userId") String userId) {
        boolean status = userDao.addUser(new UserDto(userId));
        if (status) {
            return new AddUserResponse(userId, Status.SUCCESS);
        } else {
            return new AddUserResponse(userId, Status.ERROR);
        }
    }
}
