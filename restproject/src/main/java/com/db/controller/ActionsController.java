package com.db.controller;

import com.db.dao.ActionsDao;
import com.db.dto.Action;
import com.db.model.Status;
import com.db.response.CreateActionResponse;
import com.db.response.GetActionResponse;
import com.db.response.GetAllActionResponse;
import com.db.response.GetTargetActionResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;


@RestController
public class ActionsController {
    @Autowired
    private ActionsDao actionsDao;

    @Autowired
    public ActionsController(ActionsDao actionsDao) {
        this.actionsDao = actionsDao;
    }

    @RequestMapping(value="/actions/create",method = RequestMethod.POST)
    @CrossOrigin
    public CreateActionResponse createActionResponse(@RequestBody Action action, HttpServletResponse response){
        response.setHeader("Access-Control-Allow-Origin","*");
        response.setHeader("Access-Control-Allow-Methods","POST, GET, OPTIONS, DELETE, PUT");
        response.setHeader("Access-Control-Max-Age","1000");
        response.setHeader("Access-Control-Allow-Headers","x-requested-with, Content-Type, origin, authorization, accept, client-security-token");
        if (actionsDao.create(action)) {
            return new CreateActionResponse(action.getId(), Status.SUCCESS);
        }
        return new CreateActionResponse(action.getId(),Status.ERROR);

    }
    @RequestMapping(value="/actions/{id}")
    @CrossOrigin
    public GetActionResponse getAction(@PathVariable String id,HttpServletResponse response){
        response.setHeader("Access-Control-Allow-Origin","*");
        response.setHeader("Access-Control-Allow-Methods","POST, GET, OPTIONS, DELETE, PUT");
        response.setHeader("Access-Control-Max-Age","1000");
        response.setHeader("Access-Control-Allow-Headers","x-requested-with, Content-Type, origin, authorization, accept, client-security-token");
        return new GetActionResponse(actionsDao.get(id), Status.SUCCESS);
    }

    @RequestMapping(value="/actions/targetActions/{id}")
    @CrossOrigin
    public GetTargetActionResponse getTargetActions(@PathVariable String id) {
        List<Action> targetActions = new ArrayList<Action>();
        targetActions.add(new Action("11","face","action","name"));
        targetActions.add(new Action("12","face","action","name"));
        targetActions.add(new Action("13","face","action","name"));
        targetActions.add(new Action("14","face","action","name"));
        return new GetTargetActionResponse(targetActions, Status.SUCCESS);
    }

    @RequestMapping(value="/actions")
    @CrossOrigin
    public GetAllActionResponse getAllAction(HttpServletResponse response){
        response.setHeader("Access-Control-Allow-Origin","*");
        response.setHeader("Access-Control-Allow-Methods","POST, GET, OPTIONS, DELETE, PUT");
        response.setHeader("Access-Control-Max-Age","1000");
        response.setHeader("Access-Control-Allow-Headers","x-requested-with, Content-Type, origin, authorization, accept, client-security-token");

        return new GetAllActionResponse(actionsDao.getAll(), Status.SUCCESS);

    }

    @RequestMapping(value="/actions/{id}", method = RequestMethod.DELETE)
    @CrossOrigin
    public CreateActionResponse deleteAction(@PathVariable String id, HttpServletResponse response){
        response.setHeader("Access-Control-Allow-Origin","*");
        response.setHeader("Access-Control-Allow-Methods","POST, GET, OPTIONS, DELETE, PUT");
        response.setHeader("Access-Control-Max-Age","1000");
        response.setHeader("Access-Control-Allow-Headers","x-requested-with, Content-Type, origin, authorization, accept, client-security-token");
        return new CreateActionResponse(actionsDao.removeAction(id), Status.SUCCESS);

    }

}
