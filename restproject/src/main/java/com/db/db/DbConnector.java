package com.db.db;

import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;

/**
 * Created by Rahul on 11/9/2016.
 */
@Component
public class DbConnector {
    private Connection connection;

    @PostConstruct
    public void init() {
        try {
            Class.forName("org.hsqldb.jdbcDriver");
            connection = DriverManager.getConnection("jdbc:hsqldb:hsql://localhost/autistic", "sa", "");
        }
        catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
        }
    }

    public Connection getConnection() {
        return connection;
    }

}
