package com.db;

import org.hsqldb.server.Server;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by Rahul on 11/9/2016.
 */
public class HsqlRunner {
    public void run() {
        Server server = new Server();
        server.setLogWriter(null);
        server.setSilent(true);
        server.setDatabaseName(0, "autistic");
        server.setDatabasePath(0, "file:autistic");
        server.setPort(9001);
        server.start();
    }

    public static void main(String[] args) {
        new HsqlRunner().run();
    }


}
