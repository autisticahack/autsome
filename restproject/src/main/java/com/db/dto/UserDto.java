package com.db.dto;

/**
 * Created by Rahul on 11/9/2016.
 */
public class UserDto {
    public String userId;

    public UserDto(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
