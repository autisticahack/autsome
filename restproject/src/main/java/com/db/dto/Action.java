package com.db.dto;

/**
 * Created by iDiot on 11/10/16.
 */
public class Action {
    private String id;
    private String name;
    private String face;
    private String targetAction;

    public Action() {
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setFace(String face) {
        this.face = face;
    }


    public Action(String id, String face, String action, String name) {
        this.id = id;
        this.face = face;
        this.targetAction = action;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getFace() {
        return face;
    }

    public String getTargetAction() {
        return targetAction;
    }

    public void setTargetAction(String targetAction) {
        this.targetAction = targetAction;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
