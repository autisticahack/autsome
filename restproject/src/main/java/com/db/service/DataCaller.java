package com.db.service;

import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;

/**
 * Created by Rahul on 11/9/2016.
 */
@Component
public class DataCaller {

    @PostConstruct
    public void init() {
        try {
            Class.forName("org.hsqldb.jdbcDriver");
            Connection connection = null;
            ResultSet rs = null;
            connection = DriverManager.getConnection("jdbc:hsqldb:hsql://localhost/autistic", "sa", ""); // can through sql exception
            connection.prepareStatement("drop table barcodes if exists;").execute();
            connection.prepareStatement("create table barcodes (id integer, barcode varchar(20) not null);").execute();
            connection.prepareStatement("insert into barcodes (id, barcode)"
                    + "values (1, '12345566');").execute();

            // query from the db
            rs = connection.prepareStatement("select id, barcode  from barcodes;").executeQuery();
            rs.next();
            System.out.println(String.format("ID: %1d, Name: %1s", rs.getInt(1), rs.getString(2)));
        }
        catch ( Exception e) {
            e.printStackTrace();
        }
    }

}
