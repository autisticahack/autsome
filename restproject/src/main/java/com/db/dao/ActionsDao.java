package com.db.dao;

import com.db.db.DbConnector;
import com.db.dto.Action;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


@Component
public class ActionsDao {
    private DbConnector dbConnector;

    @Autowired
    public ActionsDao(DbConnector inDbConnector){
        dbConnector = inDbConnector;
    }

    public boolean create(Action action){

        try {

            PreparedStatement preparedStatement = dbConnector.getConnection()
                    .prepareStatement("insert into action(face,action,name) values(?,?,?)");
            preparedStatement.setString(1,action.getFace());
            preparedStatement.setString(2,action.getTargetAction());
            preparedStatement.setString(3,action.getName());
            return preparedStatement.executeUpdate() > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public Action get(String id){
        try {
            PreparedStatement preparedStatement = dbConnector.getConnection().prepareStatement("select * from action where id=?");
            preparedStatement.setString(1,id);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()){
                return new Action(
                    resultSet.getString("id"),
                    resultSet.getString("face"),
                    resultSet.getString("action"),
                        resultSet.getString("name")

                    );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Action> getAll(){
        List<Action> result = new ArrayList<Action>();
        try {
            PreparedStatement preparedStatement = dbConnector.getConnection().prepareStatement("select * from action");

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()){
                result.add(new Action(
                        resultSet.getString("id"),
                        resultSet.getString("face"),
                        resultSet.getString("action"),
                        resultSet.getString("name")
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public String removeAction(String id) {
        return null;
    }
}
