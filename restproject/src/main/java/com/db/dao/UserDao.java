package com.db.dao;

import com.db.db.DbConnector;
import com.db.dto.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by Rahul on 11/9/2016.
 */
@Component
public class UserDao {

    private DbConnector dbConnector;

    @Autowired
    public UserDao(DbConnector dbConnector) {
        this.dbConnector = dbConnector;
    }

    public boolean addUser(UserDto user) {
        boolean status = false;
        try {
            dbConnector.getConnection().createStatement().execute("insert into user (user_id) values ('" + user.getUserId() + "')");
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;

    }


}
