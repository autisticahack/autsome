package com.db.dao;

import com.db.db.DbConnector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by Rahul on 11/9/2016.
 */
@Component
public class SchemaWriter {

    private DbConnector dbConnector;

    @Autowired
    public SchemaWriter(DbConnector dbConnector) {
        this.dbConnector = dbConnector;
    }

    @PostConstruct
    public void init() {
        InputStream stream = getClass().getClassLoader().getResourceAsStream("queries.txt");
        BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
        String query =null;

        try {
            while((query = reader.readLine()) !=null) {
                dbConnector.getConnection().createStatement().execute(query);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


}
