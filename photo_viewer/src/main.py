'''
Basic Picture Viewer
====================

'''

import kivy
import os
kivy.require('1.0.6')

from glob import glob
from random import randint
from os.path import join, dirname
from kivy.app import App
from kivy.logger import Logger
from kivy.uix.scatter import Scatter
from kivy.properties import StringProperty


class Picture(Scatter):
    '''Picture is the class that will show the image with a white border and a
    shadow. They are nothing here because almost everything is inside the
    picture.kv. Check the rule named <Picture> inside the file, and you'll see
    how the Picture() is really constructed and used.

    The source property will be the filename to show.
    '''

    source = StringProperty(None)
    def on_touch_down(self, touch):
        print 'on_touch_down'
        print self.parent.children[0].children[0].source
#         self.parent.children[0].children[0].
        return super(Picture, self).on_touch_down(touch)


class PicturesApp(App):

    def setValue(self):
        pass
    def build(self):
            # the root is created in pictures.kv
        root = self.root

        # get any files into images directory
#         curdir = dirname(__file__)
        imagesRootPath='/docs/LiClipse Workspace/hackathon/images'
        # get any files into images directory
        if os.path.exists(imagesRootPath):
            os.chdir(imagesRootPath)
            listOfDir = os.listdir(imagesRootPath)
#         books = FindingBook().findAllBooks()
        for imagePath in listOfDir:
            filename=str(os.path.join(imagesRootPath, str(imagePath)))
#         for filename in glob(join(curdir, 'images', '*')):
            try:
                # load the image
                picture = Picture(source=str(filename), rotation=randint(-0, 0))
                # add to the main field
                root.add_widget(picture)
            except Exception as e:
                Logger.exception('Pictures: Unable to load <%s>' % filename)

    def on_pause(self):
        return True


if __name__ == '__main__':
    pic=PicturesApp()
    pic.setValue()
    pic.run()